from typing import List
from typing import Tuple

import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="pop_create")

    hub.pop.config.load(["pop_create"], "pop_create", parse_cli=False)

    yield hub


def compare_dir_trees(
    tree1: List[Tuple[str, List[str], List[str]]],
    tree2: List[Tuple[str, List[str], List[str]]],
):
    first = lambda x: x[0]

    tree1 = sorted(tree1, key=first)
    tree2 = sorted(tree2, key=first)

    clean = lambda x: sorted(i for i in x if i != "__pycache__")
    out1 = []
    for t1 in tree1:
        if t1[0].endswith("__pycache__"):
            continue
        out1.append((t1[0], clean(t1[1]), clean(t1[2])))
    out2 = []
    for t2 in tree2:
        if t2[0].endswith("__pycache__"):
            continue
        out2.append((t2[0], clean(t2[1]), clean(t2[2])))

    assert out1 == out2
