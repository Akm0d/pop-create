import os
import pathlib
import subprocess
import sys
import tempfile
from unittest import mock

from tests.integration.conftest import compare_dir_trees


def test_cli(hub):
    with tempfile.TemporaryDirectory(prefix="test_", suffix="_pop_create") as temp_dir:
        temp_path = pathlib.Path(temp_dir)
        # Verify that the temporary directory is completely empty
        assert not os.listdir(temp_dir)

        # Run the pop-create cli with mocked arguments
        with mock.patch(
            "sys.argv",
            [
                "pop-create",
                "seed",
                "--overwrite-existing",
                f"--directory={temp_dir}",
                "--project-name=test-project",
            ],
        ):
            hub.pop_create.init.cli()

        compare_dir_trees(
            os.walk(temp_dir),
            [
                (
                    temp_dir,
                    ["requirements", "test_project"],
                    [
                        "run.py",
                        "setup.py",
                        ".gitignore",
                        ".pyproject.toml",
                        "CONTRIBUTING.rst",
                        "LICENSE",
                        "README.rst",
                    ],
                ),
                (
                    str(temp_path / "test_project"),
                    ["test_project"],
                    ["scripts.py", "version.py", "conf.py"],
                ),
                (
                    str(temp_path / "requirements"),
                    [],
                    ["base.txt"],
                ),
                (
                    str(temp_path / "test_project" / "test_project"),
                    ["contracts"],
                    ["init.py"],
                ),
                (
                    str(temp_path / "test_project" / "test_project" / "contracts"),
                    [],
                    [],
                ),
            ],
        )


def test_run(hub):
    with tempfile.TemporaryDirectory(prefix="test_", suffix="_pop_create") as temp_dir:
        # Run the pop-create cli with mocked arguments
        with mock.patch(
            "sys.argv",
            [
                "pop-create",
                "seed",
                "--overwrite-existing",
                f"--directory={temp_dir}",
                "--project-name=test-project",
            ],
        ):
            hub.pop_create.init.cli()

        path = pathlib.Path(temp_dir) / "run.py"
        output = subprocess.getoutput(f"{sys.executable} {path}")
        assert output == "test-project works!"


def test_setup_install(hub):
    cwd = os.getcwd()
    try:
        with tempfile.TemporaryDirectory(
            prefix="test_", suffix="_pop_create"
        ) as temp_dir:
            os.chdir(temp_dir)
            # Run the pop-create cli with mocked arguments
            with mock.patch(
                "sys.argv",
                [
                    "pop-create",
                    "seed",
                    "--overwrite-existing",
                    f"--directory={temp_dir}",
                    "--project-name=test-project",
                ],
            ):
                hub.pop_create.init.cli()

            errcode, output = subprocess.getstatusoutput(
                f"{sys.executable} setup.py install --dry-run"
            )

            assert not errcode, output
    finally:
        os.chdir(cwd)


def test_setup_build(hub):
    cwd = os.getcwd()
    try:
        with tempfile.TemporaryDirectory(
            prefix="test_", suffix="_pop_create"
        ) as temp_dir:
            os.chdir(temp_dir)
            # Run the pop-create cli with mocked arguments
            with mock.patch(
                "sys.argv",
                [
                    "pop-create",
                    "seed",
                    "--overwrite-existing",
                    f"--directory={temp_dir}",
                    "--project-name=test-project",
                ],
            ):
                hub.pop_create.init.cli()

            errcode, output = subprocess.getstatusoutput(
                f"{sys.executable} setup.py build"
            )

            assert not errcode, output
    finally:
        os.chdir(cwd)
