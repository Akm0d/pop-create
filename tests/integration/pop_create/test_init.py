import pathlib
import tempfile

import pytest


def test_run(hub):
    with tempfile.TemporaryDirectory() as tempdir:
        hub.pop_create.init.run(directory=tempdir, subparsers=[])


def test_run_bad_subparser(hub):
    with tempfile.TemporaryDirectory() as tempdir:
        with pytest.raises(AttributeError):
            hub.pop_create.init.run(
                directory=tempdir, subparsers=["nonexistant subparser"]
            )


def test_copytree(hub):
    with tempfile.TemporaryDirectory(prefix="pop-create-tests-", suffix="-src") as src:
        with tempfile.TemporaryDirectory(
            prefix="pop-create-tests-", suffix="-dst"
        ) as dst:
            srcdir = pathlib.Path(src)
            dstdir = pathlib.Path(dst)

            (srcdir / "new.txt").touch()
            src_exist_file = srcdir / "exist.txt"
            src_exist_file.write_text("overwritten")
            dst_exist_file = dstdir / "exist.txt"
            dst_exist_file.write_text("skipped")

            hub.pop_create.init.copytree(srcdir, dstdir)
            actual_contents = dst_exist_file.read_text()
            assert actual_contents == "skipped", actual_contents
