#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile -o requirements/py3.10/tests.txt -v requirements/tests.in
#
aiofiles==0.4.0           # via aiologger
aiologger[aiofiles]==0.6.0  # via pop-config
asynctest==0.13.0
attrs==20.3.0             # via pytest
dict-toolbox==2           # via pop, pop-config, pytest-pop
importlib-metadata==3.7.3  # via pluggy, pytest
iniconfig==1.1.1          # via pytest
mock==4.0.3
msgpack==1.0.2            # via dict-toolbox, pop, pytest-salt-factories
packaging==20.9           # via pytest
pluggy==0.13.1            # via pytest
pop-config==6.11          # via pop, pytest-pop
pop==16.3
psutil==5.8.0             # via pytest-salt-factories
py==1.10.0                # via pytest
pyparsing==2.4.7          # via packaging
pytest-asyncio==0.14.0
pytest-pop==6.3
pytest-salt-factories==0.13.0  # via pytest-pop
pytest-tempdir==2019.10.12  # via pytest-salt-factories
pytest==6.2.2
pyyaml==5.4.1             # via dict-toolbox, pop
pyzmq==22.0.3             # via pytest-salt-factories
toml==0.10.2              # via pytest
typing-extensions==3.7.4.3  # via importlib-metadata
typing==3.7.4.3           # via typing-extensions
zipp==3.4.1               # via importlib-metadata
